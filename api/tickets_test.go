package api

import (
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
)

func TestIssueTicket(t *testing.T) {
	// Setup
	f := make(url.Values)
	req := httptest.NewRequest(http.MethodPost, "/ticket", strings.NewReader(f.Encode()))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationForm)
	req.Header.Set("User-Hash", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36")
	rec := httptest.NewRecorder()

	//asserts
	assert.Equal(t, http.StatusOK, rec.Code)
}
