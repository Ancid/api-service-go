package api

import (
	"api_service/api/middleware"
	"api_service/database"
	"api_service/ticket"
	"github.com/labstack/echo/v4"
	echomiddleware "github.com/labstack/echo/v4/middleware"
	"gorm.io/gorm"
	"os"
)

func Start(db *gorm.DB) {
	e := echo.New()

	e.Use(echomiddleware.Logger())
	e.Use(echomiddleware.Recover())
	e.Use(middleware.UserCookie)

	repository := database.NewRepository(db)
	ticketService := ticket.NewService(repository)

	ticketHandler := NewTicketsHandler(ticketService)
	e.GET("/tickets", ticketHandler.Get)
	e.POST("/ticket", ticketHandler.IssueTicket)

	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}

	e.Logger.Fatal(e.Start(":" + httpPort))
}
