package api

import (
	"api_service/api/middleware"
	"api_service/ticket"
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
)

type TicketsHandler interface {
	Get(c echo.Context) error
	IssueTicket(c echo.Context) error
}

type ticketsHandler struct {
	ticketService ticket.Service
}

func NewTicketsHandler(
	ticketService ticket.Service,
) TicketsHandler {
	return ticketsHandler{
		ticketService: ticketService,
	}
}

func (t ticketsHandler) Get(c echo.Context) error {
	tickets, err := t.ticketService.GetTickets()
	if err != nil {
		return c.String(http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError))
	}

	return c.JSON(http.StatusOK, tickets)
}

func (t ticketsHandler) IssueTicket(c echo.Context) error {
	userCookie, err := c.Cookie(middleware.UserCookieName)
	fmt.Println(userCookie.Value, err)

	lotteryID := uint(1) //Supposed to get lottery from request data
	user, err := t.ticketService.GetUser(userCookie)
	if err != nil {
		return c.String(http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError))
	}

	ticketsLimitReached, _ := t.ticketService.IsTicketsLimitReached(lotteryID)
	if ticketsLimitReached == true {
		return c.String(http.StatusGone, http.StatusText(http.StatusGone))
	}

	newTicket, _ := t.ticketService.IssueTicket(lotteryID, user)
	if len(newTicket) == 0 {
		return c.String(http.StatusForbidden, http.StatusText(http.StatusForbidden))
	}

	return c.String(http.StatusOK, http.StatusText(http.StatusOK))
}
