# Golang API service coding exercise
This is a coding exercise done by Anton Sidorov

### Task description
Imagine a popular free national e-lottery website. In order to take a part in the
lottery, users should request a lottery ticket using a button.
But the number of tickets is limited so only the fastest users who click the button
can get it. Each user can get only one ticket.

### The Lottery service
Implement Lottery API service. It should be a simple HTTP server that responds
to POST /ticket:
- with HTTP 200 if the request is successful;
- with HTTP 410 if out of tickets;
- with HTTP 403 if users try to issue a ticket again.
  Note: Provisioning a database is not required for the live technical interview though
  might be considered in a real environment.


### Installing

Run docker and init project once:
```
 make build
```

run tests:
```
 make tests
```

for start and stop the project use:
```
 make up
 make down
```

### Dev notes
What can be improved?
- tests. Lack of test in this project relates to not the best structure of api handler. 
  In current structure it is pretty hard to mock services
- no performance tests were added. Add benchmarks for high load (>= 1M requests)
- use more common framework for server implementation
- API Swagger documentation can be added
