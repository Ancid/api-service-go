package database

import (
	"github.com/gofrs/uuid"
)

type User struct {
	ID      uint
	Name    string
	Hash    string `gorm:"index:idx_hash"`
	Tickets []Ticket
}

type Ticket struct {
	ID        uint
	Number    uuid.UUID
	LotteryID uint `gorm:"index:idx_lottery_user_uniq,unique"`
	UserID    uint `gorm:"index:idx_lottery_user_uniq,unique"`
}

type Lottery struct {
	ID                uint
	Type              string
	TicketsLimitTotal uint `gorm:"index:idx_tickets_limit"`
}
