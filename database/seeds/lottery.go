package seeds

import "gorm.io/gorm"

type Lottery struct {
	Name string
	Run  func(*gorm.DB) error
}
