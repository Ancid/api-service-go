package seeds

import (
	"api_service/database"
	"gorm.io/gorm"
)

func CreateLottery(db *gorm.DB, id uint, lotteryType string, ticketsLimit uint) error {
	return db.Create(
		&database.Lottery{
			ID:                id,
			Type:              lotteryType,
			TicketsLimitTotal: ticketsLimit,
		},
	).Error
}

func All() []Lottery {
	return []Lottery{
		Lottery{
			Name: "Create test lottery",
			Run: func(db *gorm.DB) error {
				return CreateLottery(db, 1, "Test lottery", 200000)
			},
		},
	}
}
