package database

import (
	"errors"
	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

type Repository interface {
	GetTickets() ([]Ticket, error)
	GetUserTicketsCount(uint, uint) (int64, error)
	CreateTicket(uint, User) (Ticket, error)
	IsTicketsLimitReached(uint) (bool, error)
	GetUser(string) (User, error)
	GetLottery(uint) (Lottery, error)
}

type repository struct {
	db *gorm.DB
}

func (t repository) GetLottery(id uint) (Lottery, error) {
	var lottery Lottery
	result := t.db.Model(&lottery).Where(&Lottery{ID: id}).First(&lottery)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		lottery = Lottery{}
	}

	return lottery, nil
}

func (t repository) GetUserTicketsCount(userId uint, lotteryID uint) (int64, error) {
	var tickets []Ticket
	var count int64
	t.db.Model(&tickets).Where(&Ticket{LotteryID: lotteryID, UserID: userId}).Count(&count)

	return count, nil
}

func (t repository) IsTicketsLimitReached(id uint) (bool, error) {
	var tickets []Ticket
	var lottery Lottery
	var count int64
	t.db.Model(&tickets).Where(&Ticket{LotteryID: id}).Count(&count)
	result := t.db.Model(&lottery).Where(&Lottery{ID: id}).First(&lottery)

	if !errors.Is(result.Error, gorm.ErrRecordNotFound) {
		if count >= int64(lottery.TicketsLimitTotal) {
			return true, nil
		}
	}

	return false, nil
}

func (t repository) CreateTicket(lotteryID uint, user User) (Ticket, error) {
	u, _ := uuid.NewV4()
	ticket := Ticket{Number: u, LotteryID: lotteryID, UserID: user.ID}
	err := t.db.Model(&ticket).Create(&ticket).Error
	if err != nil {
		return ticket, err
	}

	return ticket, nil
}

func (t repository) GetTickets() ([]Ticket, error) {
	var result []Ticket
	err := t.db.Model(&Ticket{}).Find(&result).Error

	if err != nil {
		return nil, err
	}

	return result, nil
}

func (t repository) GetUser(hash string) (User, error) {
	var user User
	result := t.db.Model(&user).Where(&User{Hash: hash}).First(&user)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		user = User{Name: "Test user", Hash: hash}
		t.db.Create(&user)
	}

	return user, nil
}

func NewRepository(db *gorm.DB) Repository {
	return repository{
		db: db,
	}
}
