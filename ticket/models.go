package ticket

import "github.com/gofrs/uuid"

type Ticket struct {
	ID        uint
	Number    uuid.UUID
	LotteryID uint
}

type User struct {
	ID   uint
	Name string
	Hash string
}
