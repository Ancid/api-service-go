package ticket

import (
	"api_service/database"
	"net/http"
)

type Service interface {
	IssueTicket(uint, User) ([]Ticket, error)
	GetTickets() ([]Ticket, error)
	GetUser(*http.Cookie) (User, error)
	IsTicketsLimitReached(uint) (bool, error)
}

type service struct {
	repository database.Repository
}

func (s service) IsTicketsLimitReached(lotteryID uint) (bool, error) {
	isLimitReached, _ := s.repository.IsTicketsLimitReached(lotteryID)

	return isLimitReached, nil
}

func (s service) IssueTicket(lotteryID uint, user User) ([]Ticket, error) {
	var tickets []Ticket
	limitReached, _ := s.repository.IsTicketsLimitReached(lotteryID)
	if limitReached == false {
		ticket, err := s.repository.CreateTicket(lotteryID, database.User{
			ID:   user.ID,
			Name: user.Name,
			Hash: user.Hash,
		})
		if err == nil {
			tickets = append(tickets, Ticket{
				ID:        ticket.ID,
				Number:    ticket.Number,
				LotteryID: ticket.LotteryID,
			})
		}
	}

	return tickets, nil
}

func (s service) GetTickets() ([]Ticket, error) {
	tickets, err := s.repository.GetTickets()
	if err != nil {
		return nil, err
	}

	result := make([]Ticket, len(tickets))
	for i, ticket := range tickets {
		result[i] = Ticket{
			ID:        ticket.ID,
			Number:    ticket.Number,
			LotteryID: ticket.LotteryID,
		}
	}

	return result, nil
}

func (s service) GetUser(cookie *http.Cookie) (User, error) {
	user, _ := s.repository.GetUser(cookie.Value)

	return User{ID: user.ID, Name: user.Name, Hash: user.Hash}, nil
}

func NewService(ticketRepository database.Repository) Service {
	return service{
		repository: ticketRepository,
	}
}
