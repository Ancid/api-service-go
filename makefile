build:
	@docker-compose up --build -d

up:
	@docker-compose up -d

down:
	@docker-compose down

remove:
	@docker-compose down -v

tests:
	@docker build -t app_test -f Dockerfile.test .
	@docker run app_test

